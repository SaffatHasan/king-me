# King Me

LAN Multiplayer Checkers Game

This respository contains the code to run a Python based checkers game using the Flask and socketio frameworks.

The game can be run through one of the two makefile targets

make
make local-run

make will run the project using a docker instance whereas make local-run assumes that you have install the libraries specified in src/requirements.txt

## Use of Version Control

The project was version controlled using Gitlab.

## Use of Static Analysis

The static analysis tools were PyLint and JsHint. To execute, run make lint

## Use of Bug Tracking Tool

The bugs were tracked within an excel sheet while maintaining a list of pertinent features such as summary,
status, and steps to reproduce. The file is called 'Issue List.xsls'

## Use of CR for Candidate Release
Refer to the release notes document.

To view prior releases please review the issues list or the releases folder. The GA is the final release noted by the file - after the demo given,

## Additional Technologies used

We additionally used GitLab continuous integration by making the master branch protected and require
all changes to be submitted in the form of a merge request. Each merge request would only be approved
if the specified unit tests, static code analysis tools, and docker image generation occurred successfully.
