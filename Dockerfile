FROM python:3.7.2-slim

WORKDIR /home/kingme

# Install git
RUN [ "apt-get", "update" ]
RUN [ "apt-get", "upgrade", "-y" ]
RUN [ "apt-get", "install", "-y", "git" ]

# Install python dependencies listed in src/requirements.txt
COPY src/requirements.txt src/requirements.txt
RUN [ "pip", "install", "--upgrade", "pip" ]
RUN [ "pip", "install", "-r", "src/requirements.txt" ]

# Copies the entire repository over
COPY . .

# Run unit tests
RUN python src/tests/test.py

# Magic number - flask app is hosted on port 5000 on the container
EXPOSE 5000

# Specifies the command on startup of the image as python src/flask/app.py
ENTRYPOINT [ "python" ]
CMD [ "src/flask/app.py" ]
